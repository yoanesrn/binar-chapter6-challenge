const express = require('express')
const app = express()
const router = express.Router();
let models = require('../models')
const sessionChecker = require('../sessionChecker/sessionChecker')



// CRUD==========================================================

router.get('/dashboard/user-game-bio', async (req, res) =>  {
    
    const allUserGameBio = await findAllUserGameBio();
   
    res.render('user-game-bio/user-game-bio',{userGameBio: allUserGameBio });
});

function findAllUserGameBio() {
    try {
        return models.UserGameBio.findAll({});   
    } catch (error) {
        console.log(error);
    }
}


router.get('/dashboard/user-game-bio/add', sessionChecker, (req, res) => {
    res.render('user-game-bio/add');
});

router.post('/dashboard/user-game-bio/add', sessionChecker, (req, res) => {
    models.UserGameBio.create({
        nama: req.body.nama,
        umur: req.body.umur,
        hobi: req.body.hobi
    })
    .then(user => {
        res.redirect('/dashboard/user-game-bio');

    })
    .catch(error => {
      console.log(error);
        res.redirect('/dashboard/user-game-bio/add');
    });
});

router.get('/dashboard/user-game-bio/edit/:id',sessionChecker, (req, res) => {
    models.UserGameBio.findByPk(req.params.id).then((userGameBio)=>{
        res.render('user-game-bio/edit',{userGameBio});
    });
});

router.put('/dashboard/user-game-bio/edit/:id', sessionChecker,(req, res) => {
    const {nama, umur,hobi} = req.body;
    models.UserGameBio.update(req.body, {
    where:{
      id:req.params.id
    }
  }).then((user) => {
    res.redirect('/dashboard/user-game-bio')
  }).catch((error)=>{console.log(error)})

});


router.delete('/dashboard/user-game-bio/delete/:id', sessionChecker,(req, res) => {
    models.UserGame.destroy({
      where:{
        id:req.params.id
      }
    }).then((user) => {  
        res.send({ msg: "Success" });
    })
});

// =======================================================



module.exports = router;
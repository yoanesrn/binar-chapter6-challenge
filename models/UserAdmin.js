/* jshint indent: 2 */
'use strict';
let bcrypt = require('bcrypt');

module.exports = (sequelize, DataTypes) => {
  let User = sequelize.define('UserAdmin', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    username: {
      type: DataTypes.STRING,
      allowNull: true
    },
    password: {
      type: DataTypes.STRING,
      allowNull: true
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
  },
  {
    hooks: {
      beforeCreate: (user) => {
        const salt = bcrypt.genSaltSync();
        user.password = bcrypt.hashSync(user.password, salt);
      }
    }
  });

  User.prototype.validPassword = function(password) {
    password = password.toString();
    return bcrypt.compare(password, this.password);
  };

  User.removeAttribute('id');

  return User;
}
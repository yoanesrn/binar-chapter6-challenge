const express = require('express');
const internalError = require('./internalError');
const notFoundhandler = require('./notFoundHandler');
const bodyParser = require('body-parser');
const session = require('express-session');
const morgan = require('morgan');
const app = express();
const port = 3000;
const router = require('./router/router');
const routerGame = require('./router/routerGame');
const userGameRouter = require('./router/userGameRouter');
const userGameBiodata = require('./router/userGameBiodataRouter');
const userGameHistoryRouter = require('./router/userGameHistoryRouter');
const cookieParser = require('cookie-parser');
const methodOverride = require('method-override');

app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json()); 
app.use(cookieParser());
app.use(methodOverride('_method'));
app.use(session({
    key: 'user_sid',
    secret: 'somerandonstuffs',
    resave: false,
    saveUninitialized: false,
    cookie: {
        expires: 600000
    }
}));

app.use((req, res, next) => {
    if (req.cookies.user_sid && !req.session.user) {
        res.clearCookie('user_sid');        
    }
    next();
});





app.use(router);
app.use(routerGame);
app.use(userGameRouter);
app.use(userGameBiodata);
app.use(userGameHistoryRouter);


app.use(internalError);

app.use(notFoundhandler);



app.listen(port,()=>{
    console.log(`this app is running in port ${port}`);
});
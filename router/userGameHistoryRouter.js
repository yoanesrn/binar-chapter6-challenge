const express = require('express')
const app = express()
const router = express.Router();
let models = require('../models')
const sessionChecker = require('../sessionChecker/sessionChecker')





router.get('/dashboard/user-game-history',sessionChecker, async (req, res) =>  {
    
  const allUserGameHistoryhistory = await findAllUserGameHistoryHistory();
 
  res.render('user-game-history/user-game-history',{userGameHistory: allUserGameHistoryhistory });
});

function findAllUserGameHistoryHistory() {
  try {
      return models.UserGameHistory.findAll({});   
  } catch (error) {
      console.log(error);
  }
}


router.get('/dashboard/user-game-history/add', sessionChecker, (req, res) => {
    res.render('user-game-history/add');
});

router.post('/dashboard/user-game-history/add/:name', sessionChecker, (req, res) => {
    models.UserGameHistory.create({
        name_player : req.params.name,
        score       : req.body.score.score.playerScore,
        playDate    :  new Date()
    })
    .then(user => {
      res.send({ msg: "Success" });

    })
});

router.get('/dashboard/user-game-history/edit/:id',sessionChecker, (req, res) => {
    models.UserGameHistory.findByPk(req.params.id).then((UserGameHistory)=>{
        res.render('user-game-history/edit',{UserGameHistory});
    });
});

router.put('/dashboard/user-game-history/edit/:id', sessionChecker,(req, res) => {
    const {username, email} = req.body;
    models.UserGameHistory.update(req.body, {
    where:{
      id:req.params.id
    }
  }).then((user) => {
    res.redirect('/dashboard/user-game-history')
  })

});

router.put('/dashboard/user-game-history/delete/:id',sessionChecker, (req, res) => {
    const {username, email} = req.body;
    models.UserGameHistory.update(req.body, {
    where:{
      id:req.params.id
    }
  }).then((user) => {
    res.redirect('/dashboard/user-game-history')
  })

});

router.delete('/dashboard/user-game-history/delete/:id', sessionChecker,(req, res) => {
    models.UserGameHistory.destroy({
      where:{
        id:req.params.id
      }
    }).then((user) => {  
        res.send({ msg: "Success" });
    })
});



module.exports = router;
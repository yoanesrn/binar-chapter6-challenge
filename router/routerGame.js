const express = require('express')
const app = express()
const router = express.Router();
let models = require('../models')
const sessionCheckerGame = require('../sessionChecker/sessionCheckerGame')








router.get('/trial-game', sessionCheckerGame, (req, res) => {
    res.render('game',{user:req.session.user});
});


// ==========================================
router.get('/login-game', (req, res) => {
    res.render('login-game');
});

router.post('/login-game', (req, res) => {
    let username = req.body.username;
    let password = req.body.password;
    models.UserGame.findOne({ where: { username: username } }).then((user) => {
        if (!user) {
            res.redirect('/login-game');
        } else if (!user.validPassword(password)) {
            res.redirect('/login-game');
        } else {
            req.session.user = user.dataValues;
            res.redirect('/trial-game');
        }
    });
});

router.get('/signup-game', (req, res) => {
    res.render('register-game');
});

router.post('/signup-game', (req, res) => {
    console.log(req.body);
    models.UserGame.create({
        username: req.body.username,
        password: req.body.password,
        email: req.body.email
    })
        .then(user => {
            req.session.user = user.dataValues;
            res.redirect('/trial-game');
        })
        .catch(error => {
            console.log(error);
            res.redirect('/signup-game');
        });
});


router.get('/logout-game', (req, res) => {
    if (req.session.user && req.cookies.user_sid) {
        res.clearCookie('user_sid');
        res.redirect('/');
    } else {
        res.redirect('/login');
    }
});

// =========================================












module.exports = router;
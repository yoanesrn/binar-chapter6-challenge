const express = require('express')
const app = express()
const router = express.Router();
let models = require('../models')
const sessionChecker = require('../sessionChecker/sessionChecker')





router.get('/', (req, res) => {
    res.render('index');
});



router.get('/dashboard', sessionChecker, (req, res) => {
    res.render('dashboard');
});


router.post('/login', (req, res) => {
    let username = req.body.username;
    let password = req.body.password;
    models.UserAdmin.findOne({ where: { username: username } }).then((user) => {
        if (!user) {
            res.redirect('/login');
        } else if (!user.validPassword(password)) {
            res.redirect('/login');
        } else {
            req.session.user = user.dataValues;
            res.redirect('/dashboard');
        }
    });
});

router.get('/login', (req, res) => {
    res.render('login');
});


router.get('/signup', (req, res) => {
    res.render('register');
});

router.post('/signup', (req, res) => {
    models.UserAdmin.create({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password
    })
        .then(user => {
            req.session.user = user.dataValues;
            res.redirect('/dashboard');
        })
        .catch(error => {
            res.redirect('/signup');
        });
});

router.get('/logout', (req, res) => {
    if (req.session.user && req.cookies.user_sid) {
        res.clearCookie('user_sid');
        res.redirect('/');
    } else {
        res.redirect('/login');
    }
});











module.exports = router;
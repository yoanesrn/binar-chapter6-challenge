/* jshint indent: 2 */
'use strict';
module.exports = (sequelize, DataTypes) => {
  let UserGameBio = sequelize.define('UserGameBio', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    nama: {
      allowNull: true,
      type: DataTypes.STRING
    },
    umur: {
      allowNull: true,
      type: DataTypes.STRING
    },
    hobi: {
      allowNull: true,
      type: DataTypes.STRING
    },
    user_id: {
      allowNull: true,
      type: DataTypes.INTEGER
    },
  },{
  })

  UserGameBio.associate = function(models) {
    UserGameBio.belongsTo(models.UserGame, {foreignKey: 'user_id', as: 'userGame'})
  };
  return UserGameBio;
}
/* jshint indent: 2 */
'use strict';
module.exports = (sequelize, DataTypes) => {
  let UserGameHistory = sequelize.define('UserGameHistory', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    name_player: {
      allowNull: true,
      type: DataTypes.STRING
    },
    status: {
      allowNull: true,
      type: DataTypes.STRING
    },
    score: {
      allowNull: true,
      type: DataTypes.STRING
    },
    playDate: {
      allowNull: true,
      type: DataTypes.DATE
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  })

  UserGameHistory.associate = function(models) {
    UserGameHistory.belongsTo(models.UserGame, {foreignKey: 'user_id', as: 'userGame'})
  };

  return UserGameHistory;
}
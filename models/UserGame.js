/* jshint indent: 2 */
'use strict';

let bcrypt = require('bcrypt');

module.exports = (sequelize, DataTypes) => {
  let UserGame = sequelize.define('UserGame', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    username: {
      type: DataTypes.STRING,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false
    },
    password: {
      allowNull: false,
      type: DataTypes.STRING
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  },
  {
    hooks: {
      beforeCreate: (user) => {
        const salt = bcrypt.genSaltSync();
        user.password = bcrypt.hashSync(user.password, salt);
      }
    }
  }
  );

  UserGame.prototype.validPassword = function(password) {
    password = password.toString();
    return bcrypt.compare(password, this.password);
  };


  return UserGame;
}